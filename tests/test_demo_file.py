import unittest

from hamcrest import assert_that, equal_to
from src.demo_file import sample_function, sample_function2

class test_class(unittest.TestCase):
    def test_sample_function(self):
        print("This is a test function")
        assert_that(sample_function(), equal_to(0))
        
    def test_sample_function2(self):
        print("This is a test function2")
        assert_that(sample_function2(), equal_to(0))

if __name__ == "__main__":
    unittest.main()
