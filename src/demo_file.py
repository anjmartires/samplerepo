def sample_function():
    print("This is a function")
    return 0

def sample_function2():
    print("This is a second function hello")
    return 0
    
if __name__ == "__main__":
    print("This is a sample python file")
    sample_function()
    exit(0)
